/* 
 * File:   patch_jobs.h
 * Author: tolyan
 *
 * Created on 27 ???? 2017 ?., 15:53
 */

#ifndef PATCH_JOBS_H
#define	PATCH_JOBS_H

#include <stdint.h>

union ic_24L26_address {
    uint16_t address;
    struct {
        uint8_t addrL;
        uint8_t addrH;
    };
};

struct sPatchJob {
    union ic_24L26_address start_addr;
    const uint8_t *pData;
    uint8_t size;
};

extern const struct sPatchJob* const Jobs[];

#endif	/* PATCH_JOBS_H */

