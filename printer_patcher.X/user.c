/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include <assert.h>

#include "config.h"
#include "system.h"
#include "ht_i2c_master.h"
#include "patch_jobs.h"

#include "user.h"

/******************************************************************************/

#define IC_24LC64_BASE          0b10100000
#define IC_ADDR_SELECTED        0b100

#define SLAVEAADDR(RW)          (IC_24LC64_BASE | (IC_ADDR_SELECTED << 1) | RW)

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

static void WriteDataBlock(const struct sPatchJob* const job) {
    assert(job->size < 32);

#ifdef GROUP_WRITE
    while (true) {
        I2CStart();
        if (I2CSend(SLAVEAADDR(I2C_WRITE))) {
            I2CSend(job->start_addr.addrH);
            I2CSend(job->start_addr.addrL);

            for (uint8_t i = 0; i < job->size; ++i) {
                assert(I2CSend(job->pData[i]))
                CLRWDT();
            }

            I2CStop();
            return;
        }
        I2CStop();
        CLRWDT();
        __delay_ms(1);
        CLRWDT();
    }
#else
    for (uint8_t i = 0; i < job->size; ++i) {
        union ic_24L26_address addr;
        addr.address = job->start_addr.address + i;
        I2CStart();
        while (!I2CSend(SLAVEAADDR(I2C_WRITE))) {
            I2CStop();
            CLRWDT();
            __delay_ms(1);
            CLRWDT();
            I2CStart();
        }
        assert(I2CSend(addr.addrH));
        assert(I2CSend(addr.addrL));
        assert(I2CSend(job->pData[i]));
        I2CStop();
    }
#endif
}

void InitApp() {
    /* Initialize User Ports/Peripherals/Project here */
    
    // No interrupt on change
    IOCAPbits.IOCAP = 0; 
    IOCANbits.IOCAN = 0; 
    
    // fixed voltafe reference is off
    FVRCONbits.FVREN = 0;
    
    // ADC off
    ADCON0bits.CHS = 0;
    ADCON0bits.ADON = 0;
    
    // DAC off
    DACCON0bits.DACEN = 0;
    DACCON0bits.DACOE = 0;

    // SR Latch off
    SRCON0bits.SRLEN = 0;

    // comparator off
    CM1CON0bits.C1ON = 0;
    CM1CON0bits.C1OE = 0;

    // Timers off
    T1CONbits.TMR1ON = 0;
    T2CONbits.TMR2ON = 0;

    // DSM off
    MDCONbits.MDEN = 0;
    MDCONbits.MDOE = 0;

    // ECCP off
    CCP1CONbits.CCP1M = 0;

    // UART off
    RCSTAbits.SPEN = 0;

    // CPS off
    CPSCON0bits.CPSON = 0;

    /* Initialize peripherals */
    I2CInit();
}

void PatchEEPROM() {
    uint16_t i = 0;
    while(true) {
        struct sPatchJob const *job = Jobs[i];
        if (job->size == 0)
            break;
        WriteDataBlock(job);
        i++;
    }
}

void Deinit() {
    I2CDeinit();
}