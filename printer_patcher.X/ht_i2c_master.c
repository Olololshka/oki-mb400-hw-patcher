/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include "ht_i2c_master.h"

/*
Function: I2CInit
Return:
Arguments:
Description: Initialize I2C in master mode, Sets the required baudrate
 */
void I2CInit(void) {
    /* SDA and SCL as input pin */
    TRISA1 = 1; 
    TRISA2 = 1;
    
    SSP1STATbits.SMP = 1; // Slew rate disabled
    SSP1CON1bits.SSPM = 0b1000; // I2C Master mode, clock = FOSC/(4 * (SSPADD + 1))
    SSP1ADDbits.SSPADD = 4; // clock devider
    SSP1CON1bits.SSPEN = 1; // enable module
}

void I2CDeinit() {
    SSP1CON1bits.SSPEN = 0;
}

/*
Function: I2CStart
Return:
Arguments:
Description: Send a start condition on I2C Bus
 */
void I2CStart() {
    SSP1CON2bits.SEN = 1; /* Start condition enabled */
    while (SSP1CON2bits.SEN); /* automatically cleared by hardware */
    /* wait for start condition to finish */
}

/*
Function: I2CStop
Return:
Arguments:
Description: Send a stop condition on I2C Bus
 */
void I2CStop() {
    SSP1CON2bits.PEN = 1; /* Stop condition enabled */
    while (SSP1CON2bits.PEN); /* Wait for stop condition to finish */
    /* PEN automatically cleared by hardware */
}

/*
Function: I2CRestart
Return:
Arguments:
Description: Sends a repeated start condition on I2C Bus
 */
void I2CRestart() {
    SSP1CON2bits.RSEN = 1; /* Repeated start enabled */
    while (SSP1CON2bits.RSEN); /* wait for condition to finish */
}

/*
Function: I2CAck
Return:
Arguments:
Description: Generates acknowledge for a transfer
 */
void I2CAck() {
    SSP1CON2bits.ACKDT = 0; /* Acknowledge data bit, 0 = ACK */
    SSP1CON2bits.ACKEN = 1; /* Ack data enabled */
    while (SSP1CON2bits.ACKEN); /* wait for ack data to send on bus */
}

/*
Function: I2CNck
Return:
Arguments:
Description: Generates Not-acknowledge for a transfer
 */
void I2CNak() {
    SSP1CON2bits.ACKDT = 1; /* Acknowledge data bit, 1 = NAK */
    SSP1CON2bits.ACKEN = 1; /* Ack data enabled */
    while (SSP1CON2bits.ACKEN); /* wait for ack data to send on bus */
}

/*
Function: I2CWait
Return:
Arguments:
Description: wait for transfer to finish
 */
void I2CWait() {
    while ((SSPCON2 & 0x1F) || (SSP1STATbits.R_nW));
    /* wait for any pending transfer */
}

/*
Function: I2CSend
Return: ACK - true, NAK - false
Arguments: dat - 8-bit data to be sent on bus
           data can be either address/data byte
Description: Send 8-bit data on I2C bus
 */
bool I2CSend(unsigned char dat) {
    SSP1BUF = dat; /* Move data to SSPBUF */
    while (SSP1STATbits.BF); /* wait till complete data is sent from buffer */
    I2CWait(); /* wait for any pending transfer */
    return !SSP1CON2bits.ACKSTAT;
}

/*
Function: I2CRead
Return:    8-bit data read from I2C bus
Arguments:
Description: read 8-bit data from I2C bus
 */
unsigned char I2CRead(void) {
    unsigned char temp;
    /* Reception works if transfer is initiated in read mode */
    SSP1CON2bits.RCEN = 1; /* Enable data reception */
    while (!SSP1STATbits.BF); /* wait for buffer full */
    temp = SSP1BUF; /* Read serial buffer and store in temp register */
    I2CWait(); /* wait to check any pending transfer */
    return temp; /* Return the read data from bus */
}
