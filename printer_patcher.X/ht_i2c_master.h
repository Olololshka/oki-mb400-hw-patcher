#ifndef HT_I2C_MASTER_H
#define	HT_I2C_MASTER_H

#include <stdbool.h>

#define I2C_READ                1
#define I2C_WRITE               0

/******************************************************************************/
/* Function Prototypes                                                 */
/******************************************************************************/

void I2CInit();
void I2CDeinit();
void I2CStart();
void I2CStop();
void I2CRestart();
void I2CAck();
void I2CNak();
void I2CWait();
bool I2CSend(unsigned char dat);
unsigned char I2CRead(void);

#endif	/* HT_I2C_MASTER_H */

