/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#include "system.h"

/* Refer to the device datasheet for information about available
oscillator configurations and to compiler documentation for macro details. */
void ConfigureOscillator(void)
{

    /*If the PIC12 device has an OSCCAL value, the HiTech Compiler provides
    a macro called _READ_OSCCAL_DATA which can be loaded using this: */

    /* TODO Configure OSCCAL if the device has an OSCCAL register */

#if 0

    OSCCAL=_READ_OSCCAL_DATA(); /* _READ_OSCCAL_DATA macro unloads cal memory */

#endif

    /*Not all PIC12 devices require this.

    /* TODO Add clock switching code if appropriate.  */

    /* Typical actions in this function are to tweak the oscillator tuning
    register, select new clock sources, and to wait until new clock sources
    are stable before resuming execution of the main project. */

    // set wochdog timer on Interval 8 ms
    WDTCONbits.WDTPS = 0b00011;

    CLRWDT();

    // 4MHz internal clock
    OSCCONbits.IRCF = 0b1101;
}


static void delay_200ms() {
    CLRWDT();
    for (uint8_t i = 0; i < 40; ++i) {
        __delay_ms(5);
        CLRWDT();
    }
}

void _fassert(int, const char *, const char *) {

    // init led
    TRISA5 = 0;
    LATA5 = 0;

    while(true) {
        // blink led
        delay_200ms();
        LATA5 = !LATA5;
    }
}