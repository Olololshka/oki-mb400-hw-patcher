
#include "config.h"
#include "patch_jobs.h"

static const uint8_t const testjob_data[]  = { 0, 1, 2, 3, 4, 5 };
static const struct sPatchJob const testjob      = { 0x0000, testjob_data,  sizeof(testjob_data)  };

//==============================================================================

#ifdef NO_JOBS_OVERFLOW

static const uint8_t const segment0_data0[] = { 0x1D, 00, 00, 00 };
static const uint8_t const segment0_data1[] = { 00, 00, 00, 0x80, 00, 00 };

static const uint8_t const segment1_data0[] = { 00, 00, 00, 00, 00, 00 };
static const uint8_t const segment1_data1[] = { 00, 00, 00 };

static const uint8_t const segment2_data0[] = { 00, 00 };
static const uint8_t const segment2_data1[] = { 00, 00, 00, 0x08, 00, 00, 00 };

static const uint8_t const segment3_data[] = { 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 };

static const uint8_t const segment4_data0[] = { 00, 00, 00 };
static const uint8_t const segment4_data1[] = { 00, 00, 00, 00, 00 };

static const uint8_t const segment5_data0[] = { 0x0D, 0x40, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 };
static const uint8_t const segment5_data1[] = { 00, 00, 00, 00, 00, 00, 00, 00, 00 };

static const uint8_t const segment6_data0[] = { 00, 00, 00, 00, 00, 00 };
static const uint8_t const segment6_data1[] = { 00 };

static const uint8_t const segment7_data0[] = { 00, 00, 00, 0x85, 0x2A, 00, 00 };
static const uint8_t const segment7_data1[] = { 00, 00, 00, 00, 00, 00, 00 };

static const uint8_t const segment8_data0[] = { 00, 00 };
static const uint8_t const segment8_data1[] = { 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 };

static const uint8_t const segment9_data[] =  { 00, 00, 00, 0x09, 0x02, 00, 00, 00 };

//==============================================================================

static const struct sPatchJob const segment00_job = { 0x002C, segment0_data0, sizeof(segment0_data0) };
static const struct sPatchJob const segment01_job = { 0x0030, segment0_data1, sizeof(segment0_data1) };

static const struct sPatchJob const segment10_job = { 0x1C1A, segment1_data0, sizeof(segment1_data0) };
static const struct sPatchJob const segment11_job = { 0x1C20, segment1_data1, sizeof(segment1_data1) };

static const struct sPatchJob const segment20_job = { 0x1C3E, segment2_data0, sizeof(segment2_data0) };
static const struct sPatchJob const segment21_job = { 0x1C40, segment2_data1, sizeof(segment2_data1) };

static const struct sPatchJob const segment3_job =  { 0x1C61, segment3_data, sizeof(segment3_data) };

static const struct sPatchJob const segment40_job = { 0x1C7D, segment4_data0, sizeof(segment4_data0) };
static const struct sPatchJob const segment41_job = { 0x1C80, segment4_data1, sizeof(segment4_data1) };

static const struct sPatchJob const segment50_job = { 0x1CB2, segment5_data0, sizeof(segment5_data0) };
static const struct sPatchJob const segment51_job = { 0x1CC0, segment5_data1, sizeof(segment5_data1) };

static const struct sPatchJob const segment60_job = { 0x1CCA, segment6_data0, sizeof(segment6_data0) };
static const struct sPatchJob const segment61_job = { 0x1CD0, segment6_data1, sizeof(segment6_data1) };

static const struct sPatchJob const segment70_job = { 0x1CF9, segment7_data0, sizeof(segment7_data0) };
static const struct sPatchJob const segment71_job = { 0x1D00, segment7_data1, sizeof(segment7_data1) };

static const struct sPatchJob const segment80_job = { 0x1EEE, segment8_data0, sizeof(segment8_data0) };
static const struct sPatchJob const segment81_job = { 0x1EF0, segment8_data1, sizeof(segment8_data1) };

static const struct sPatchJob const segment9_job =  { 0x1e83, segment9_data, sizeof(segment9_data) };

static const struct sPatchJob const empy_job = { 0, 0, 0 };

//==============================================================================

const struct sPatchJob* const Jobs[] = {
    //&testjob,
    &segment00_job, &segment01_job,
    &segment10_job, &segment11_job,
    &segment20_job, &segment21_job,
    &segment3_job,
    &segment40_job, &segment41_job,
    &segment50_job, &segment51_job,
    &segment60_job, &segment61_job,
    &segment70_job, &segment71_job,
    &segment80_job, &segment81_job,
    &segment9_job,
    &empy_job
};


#else

static const uint8_t const segment0_data[] = { 0x1D, 00, 00, 00, 00, 00, 00, 0x80, 00, 00 };
static const uint8_t const segment1_data[] = { 00, 00, 00, 00, 00, 00, 00, 00, 00 };
static const uint8_t const segment2_data[] = { 00, 00, 00, 00, 00, 0x08, 00, 00, 00 };
static const uint8_t const segment3_data[] = { 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 };
static const uint8_t const segment4_data[] = { 00, 00, 00, 00, 00, 00, 00, 00 };
static const uint8_t const segment5_data[] = { 0x0D, 0x40, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 };
static const uint8_t const segment6_data[] = { 00, 00, 00, 00, 00, 00, 00 };
static const uint8_t const segment7_data[] = { 00, 00, 00, 0x85, 0x2A, 00, 00, 00, 00, 00, 00, 00, 00 };
static const uint8_t const segment8_data[] = { 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 };

//==============================================================================

static const struct sPatchJob const segment0_job = { 0x002C, segment0_data, sizeof(segment0_data) };
static const struct sPatchJob const segment1_job = { 0x1C1A, segment1_data, sizeof(segment1_data) };
static const struct sPatchJob const segment2_job = { 0x1C3E, segment2_data, sizeof(segment2_data) };
static const struct sPatchJob const segment3_job = { 0x1C61, segment3_data, sizeof(segment3_data) };
static const struct sPatchJob const segment4_job = { 0x1C7D, segment4_data, sizeof(segment4_data) };
static const struct sPatchJob const segment5_job = { 0x1CB2, segment5_data, sizeof(segment5_data) };
static const struct sPatchJob const segment6_job = { 0x1CCA, segment6_data, sizeof(segment6_data) };
static const struct sPatchJob const segment7_job = { 0x1CF9, segment7_data, sizeof(segment7_data) };
static const struct sPatchJob const segment8_job = { 0x1EEE, segment8_data, sizeof(segment8_data) };

static const struct sPatchJob const empy_job = { 0, 0, 0 };

//==============================================================================

const struct sPatchJob* const Jobs[] = {
    //&testjob,
    &segment0_job,
    &segment1_job,
    &segment2_job,
    &segment3_job,
    &segment4_job,
    &segment5_job,
    &segment6_job,
    &segment7_job,
    &segment8_job,
    &empy_job
};

#endif